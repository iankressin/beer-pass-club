import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NglModule } from 'ng-lightning';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RequesterFormComponent } from './requester-form/requester-form.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabaseModule  } from '@angular/fire/database';
import { environment  } from '../environments/environment';

import { ReactiveFormsModule } from '@angular/forms';
import { ListRequestsComponent } from './list-requests/list-requests.component';

import { RouterModule, Routes } from '@angular/router';
import { MenuComponent } from './menu/menu.component';

const appRoutes: Routes = [
  {path: 'new-service-request', component: RequesterFormComponent},
  {path: 'list-requests', component: ListRequestsComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    RequesterFormComponent,
    ListRequestsComponent,
    MenuComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    NglModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
